import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessPermisionsComponent } from './access-permisions.component';

describe('AccessPermisionsComponent', () => {
  let component: AccessPermisionsComponent;
  let fixture: ComponentFixture<AccessPermisionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessPermisionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessPermisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
