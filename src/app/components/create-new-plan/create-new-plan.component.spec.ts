import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewPlanComponent } from './create-new-plan.component';

describe('CreateNewPlanComponent', () => {
  let component: CreateNewPlanComponent;
  let fixture: ComponentFixture<CreateNewPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateNewPlanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
