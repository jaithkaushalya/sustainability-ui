import { Component, OnInit, Inject, Optional, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface Category {
  id: number;
  categoryName: string;
  target: string;
  actual: string;
  jan: string;
  feb: string;
  mar: string;
  apr: string;
  may: string;
  jun: string;
  jul: string;
  aug: string;
  sep: string;
  oct: string;
  nov: string;
  dec: string;
}

const tickets: Category[] = [
  {
    id: 1,
    categoryName: 'Water Usage (Gallons)',
    target: '12000',
    actual: '2300',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 2,
    categoryName: 'Electricity Usage (Kwh)',
    target: '8000',
    actual: '5000',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 3,
    categoryName: 'Carbon foot print (MT/MS)',
    target: '1000',
    actual: '2000',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 4,
    categoryName: 'Climate protection (LK)',
    target: '7800',
    actual: '5000',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 5,
    categoryName: 'Pollution Prevention (df)',
    target: '2100',
    actual: '3800',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 6,
    categoryName: 'Education (UI)',
    target: '7690',
    actual: '5677',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 7,
    categoryName: 'Human Health (JH)',
    target: '1090',
    actual: '2309',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 8,
    categoryName: 'Employment (WT)',
    target: '3400',
    actual: '1230',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 9,
    categoryName: 'Productivity (NM)',
    target: '3400',
    actual: '2300',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
  {
    id: 10,
    categoryName: 'Resource Efficiency (W)',
    target: '2100',
    actual: '2300',
    jan: '0.00',
    feb: '0.00',
    mar: '0.00',
    apr: '0.00',
    may: '0.00',
    jun: '0.00',
    jul: '0.00',
    aug: '0.00',
    sep: '0.00',
    oct: '0.00',
    nov: '0.00',
    dec: '0.00'
  },
];


@Component({
  selector: 'app-environmental',
  templateUrl: './environmental.component.html',
  styleUrls: ['./environmental.component.scss']
})
export class EnvironmentalComponent implements OnInit {

  @ViewChild(MatTable, { static: true }) table: MatTable<any> = Object.create(null);
  searchText: any;

  displayedColumns: string[] = ['category', 'target', 'actual', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul',
  'aug', 'sep', 'oct', 'nov', 'dec'];
  dataSource = new MatTableDataSource(tickets);


  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(tickets);
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  btnCategoryClick(val: string) {
    this.dataSource.filter = val.trim().toLowerCase();
    return this.dataSource.filteredData.length;

  }

}

