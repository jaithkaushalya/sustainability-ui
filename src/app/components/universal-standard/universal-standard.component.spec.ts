import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversalStandardComponent } from './universal-standard.component';

describe('UniversalStandardComponent', () => {
  let component: UniversalStandardComponent;
  let fixture: ComponentFixture<UniversalStandardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UniversalStandardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversalStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
