import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllOpenActionsComponent } from './all-open-actions.component';

describe('AllOpenActionsComponent', () => {
  let component: AllOpenActionsComponent;
  let fixture: ComponentFixture<AllOpenActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllOpenActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllOpenActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
