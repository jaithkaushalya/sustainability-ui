import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportExtractComponent } from './report-extract.component';

describe('ReportExtractComponent', () => {
  let component: ReportExtractComponent;
  let fixture: ComponentFixture<ReportExtractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportExtractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportExtractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
