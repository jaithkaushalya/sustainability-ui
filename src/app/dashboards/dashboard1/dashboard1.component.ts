import { Component } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {StandardDialogComponent} from '../../components/dialogs/standard-dialog/standard-dialog.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.scss']
})
export class Dashboard1Component {
  economics = [
    {
      title: 'Launch new template',
      description: 'Integer posuere erat a ante venenatis dapibus posuere.',
      icon: 'info_outline'
    },
    {
      title: 'Book a Ticket',
      description: 'Blandit tempus porttitor aasfs.',
      icon: 'info_outline'
    },
    {
      title: 'Task review',
      description: 'Lorem Ipsum, dapibus ac facilisis in, egestas eget quam. Integer posuere erat.',
      icon: 'info_outline'
    }
  ];

  environmental = [
    {
      title: 'Carbon Foot Print',
      description: 'Integer posuere erat a ante venenatis dapibus posuere.',
      icon: 'autorenew',
    },
    {
      title: 'Water Usage',
      description:
        'Lorem Ipsum, dapibus ac facilisis in, egestas eget quam. Integer posuere erat aassg.',
      icon: 'autorenew',
    },
    {
      title: 'Electricity Usage',
      description: 'Lorem Ipsum, dapibus ac facilisis in',
      icon: 'autorenew',
    },
  ];

  social = [
    {
      title: 'Design work',
      description:
        'Commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.',
      icon: 'history'
    },
    {
      title: 'Meeting with team',
      description: 'Dapibus ac facilisis in, egestas eget quam.',
      icon: 'history'
    },
    {
      title: 'Material Pro angular',
      description: 'We have finished working on MaterialPro',
      icon: 'history'
    },
  ];

  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(StandardDialogComponent, {
      width: '750px',
      data: { }
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed');
    //   this.animal = result;
    // });
  }
}
