import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface Saperator {
  name: string;
  type?: string;
}

export interface SubChildren {
  state: string;
  name: string;
  type?: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  child?: SubChildren[];
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '',
    name: '',
    type: 'saperator',
    icon: 'av_timer'
  },
  {
    state: 'dashboard',
    name: 'Dashboard',
    type: 'link',
    icon: 'av_timer',
  },
  {
    state: 'environmental',
    name: 'Data Entry',
    type: 'link',
    icon: 'ac_unit',
  },
  {
    state: 'universal',
    name: 'Universal Standards',
    type: 'link',
    icon: 'language',
  },

  {
    state: '',
    name: '',
    type: 'saperator',
    icon: 'av_timer'
  },


  {
    state: 'create-new-plan',
    name: 'Create New Plan',
    type: 'link',
    icon: 'add_to_photos',
  },
  {
    state: 'report-extract',
    name: 'Report Extract',
    type: 'link',
    icon: 'insert_chart',
  },
  {
    state: 'all-open-actions',
    name: 'All Open Actions',
    type: 'link',
    icon: 'folder',
  },
  {
    state: 'access-permision',
    name: 'Access/Permisions',
    type: 'link',
    icon: 'lock',
  },


];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
